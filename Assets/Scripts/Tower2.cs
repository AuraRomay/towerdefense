﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tower2 : MonoBehaviour {
	public int state;
	//public int distance;
	//public GameObject bullet;
	//public GameObject cube;

	public GameObject[] targets;	//Almaceno los minions dentro del Trigger
	//public GameObject[] heroes; 
	//public int shotCooldown;		//Cuanto se tarda para disparar

	//private int currentCD;			//Cuente el CoolDown 
	//private bool canShoot;			//Hay minions en el trigger?
	private int toDie = 0;			//Límite de minions para que la torre se destruya
	//public int vida;


	void Update () {

		if (toDie == targets.Length) {      			//Se destruyó torreta
			state = 0;									//Ignoran la torre

			for (int i = 0; i < targets.Length; i++) {	//Para que las siguientes torres los encuentren
				if (targets [i] != null) {
					targets [i].GetComponent<Minion> ().tagged = false;
				}
			}
			gameObject.SetActive (false);
			LoadScene ();
		}
	}

	void OnTriggerEnter (Collider other) {
		if (other.transform.tag == "minion") {
			//canShoot = true;

			for (int i = 0; i < targets.Length; i++) {
				if (targets [i] == null && !other.GetComponent<Minion>().tagged) {
					targets [i] = other.gameObject;
					other.GetComponent<Minion> ().tagged = true;
					toDie++;
				}
			}
		} 
	}

	public void LoadScene(){ //GAMEOBJECT
		SceneManager.LoadScene ("Over");	//Te lleva al Nivel
	}

	/*void OnTriggerStay(Collider other) {
		 if (other.transform.tag == "minion") {
			for (int i = 0; i < targets.Length; i++)
			{
				if (targets[i] != null && other.GetComponent<Minion>().ded)
				{
					DestroyObject(targets[i]);
					other.GetComponent<Minion>().tagged = false;
					toDie--;
					Debug.Log ("DestroyedMinion");
					break;
				}
			}
		}

	}*/
}
