﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinMins : MonoBehaviour {
	
	public float time;

	IEnumerator Win()
	{
		while (true)
		{
			yield return new WaitForSeconds(time);
			LoadScene ();
		}  

	}

	// Use this for initialization
	void Start () {
		StartCoroutine(Win());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoadScene(){ //GAMEOBJECT
		SceneManager.LoadScene ("Ganaste");	//Te lleva al Nivel
	}
}
