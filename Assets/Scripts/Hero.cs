﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Hero : MonoBehaviour {

	private NavMeshAgent nav;
	private Animator anim;

	//public bool tagged = false;
	//public bool ded = false;
	//public int vida;
	//public Bullet bull;
	//public int qDamage;
	//public bool qAttacked = false;

	// Use this for initialization
	void Start () {
		
		nav = GetComponent<NavMeshAgent> ();
		anim = GetComponent<Animator> ();
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown ("Fire1")) {

			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit, 100)) {
				if (hit.point != null) {
					Debug.Log("dispara");
					nav.SetDestination (hit.point);
				}
			}
		}

		if (Input.GetKeyDown (KeyCode.Q) && !anim.GetCurrentAnimatorStateInfo(0).IsName("RoundKick")) {
			Debug.Log ("input");
			anim.SetBool ("attack", true);
			//qAttacked = true;
		}else if(anim.GetCurrentAnimatorStateInfo(0).IsName("Run")){
			anim.SetBool ("attack", false);
			//qAttacked = false;
			//Debug.Log ("unattack");
		}

		//Si su vida es cero
	}

	void OnTriggerEnter(Collider other){
		if (other.transform.tag == "bullet") {
			//vida -= other.GetComponent<Bullet>().damage;
			Destroy (other.gameObject);
		}
	}

	/*void OnTriggerStay(Collider other){
		if (other.transform.tag == "tower" && qAttacked) {
			other.GetComponent<Tower>().vida -= qDamage;
			Debug.Log ("Seconds");
		}
	}*/

}
