﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	private Transform target;	//A donde va la bala

	public int damage;

	// Use this for initialization
	void Start () {

	}

	public void setTarget(Transform _target){		//Función para llamarlo a otro script. Lo viste en asesoría
		target = _target;
	}

	// Update is called once per frame
	void Update () {

		if (target != null) {						//Existe a donde vas?
			transform.position = Vector3.Lerp (transform.position, target.position, Time.time * 0.01f); // move towards target  Lerp- función en el update pos inicial, pos final y cuanto se tarda en llegar(usas tiempo)
		} else {

			Destroy (gameObject);
		}
	}
}
	
