﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CUbe : MonoBehaviour {

	public GameObject tower;

	// Use this for initialization
	void Start () {
		
	}
	
	void OnTriggerStay(Collider other){
		if (other.transform.tag == "hero") {
			tower.GetComponent<Tower>().canShoot = true;
		}
	}
	void OnTriggerExit(Collider other){
		if (other.transform.tag == "hero") {
			tower.GetComponent<Tower>().canShoot = false;
		}
	}
}
