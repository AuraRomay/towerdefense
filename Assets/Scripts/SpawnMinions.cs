﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMinions : MonoBehaviour {

    public bool flag;
    public float time;
    public GameObject[] towers;
   
    public GameObject prefabMinion;
    public int waveSize;
	//public string line;

    IEnumerator spawn()
    {
        while (true)
        {
			yield return new WaitForSeconds (time);
            for (int i = 0; i < waveSize; i++)
            {
                yield return new WaitForSeconds(time/ waveSize);
                GameObject min=(GameObject)Instantiate(prefabMinion, transform.position, Quaternion.identity);
                Minion scriptMin=min.GetComponent<Minion>();
                scriptMin.Towers = towers;
            }
        }  
    }
    // Use this for initialization
	void Start () {
        StartCoroutine(spawn());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
