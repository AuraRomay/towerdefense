﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {
    public int state;
    //public int distance;
	public GameObject bullet;
	//public GameObject cube;

	public GameObject[] targets;	//Almaceno los minions dentro del Trigger
	public int shotCooldown;		//Cuanto se tarda para disparar

	private int currentCD;			//Cuente el CoolDown 
	public bool canShoot;			//Hay minions en el trigger?
	//private int toDie = 0;			//Límite de minions para que la torre se destruya

	
	void Update () {
       
		/*if (toDie == targets.Length) {      			//Se destruyó torreta
			state = 0;									//Ignoran la torre

			for (int i = 0; i < targets.Length; i++) {	//Para que las siguientes torres los encuentren
				if (targets [i] != null) {
					targets [i].GetComponent<Minion> ().tagged = false;
				}
			}
			gameObject.SetActive (false);
		}*/

		if (currentCD < shotCooldown && canShoot) {
			currentCD++;
		} else if (currentCD >= shotCooldown && canShoot) {
			currentCD = 0;

			for (int m = 0; m < targets.Length; m++) {
				if (targets [m] != null) {
					GameObject b = (GameObject)Instantiate (bullet, transform.position + new Vector3 (0, 10, 0), targets [m].transform.rotation);
					Bullet bull = b.GetComponent<Bullet> ();
					bull.setTarget (targets [m].transform);
					break;
				}
			}
		}
			
	}

	void OnTriggerEnter (Collider other) {
		if (other.transform.tag == "minion") {

			for (int i = 0; i < targets.Length; i++) {
				if (targets [i] == null && !other.GetComponent<Minion> ().tagged) {
					targets [i] = other.gameObject;
					other.GetComponent<Minion> ().tagged = true;
					//toDie++;
				}
			}
		} 
	}
	void OnTriggerExit (Collider other) {
		if (other.transform.tag == "minion") {

			for (int i = 0; i < targets.Length; i++) {
				if (targets [i] != null && other.GetComponent<Minion> ().tagged) {
					targets [i] = other.gameObject;
					other.GetComponent<Minion> ().tagged = false;
					//toDie++;
				}
			}
		} 
	}


    void OnTriggerStay(Collider other) {

		if (other.transform.tag == "minion") {
			for (int i = 0; i < targets.Length; i++)
			{
				if (targets[i] != null && other.GetComponent<Minion>().ded)
				{
					DestroyObject(targets[i]);
					other.GetComponent<Minion>().tagged = false;
					//toDie--;
					break;
				}
			}
        }

    }
}
/*
GameObject[] minions = GameObject.FindGameObjectsWithTag("minion");
GameObject[]heroes = GameObject.FindGameObjectsWithTag("hero");

bool shoot=false;

for(int i=0; i<heroes.Length; i++){
	if(Vector3.Distance(transform.position, heroes[i].transform.position) < distance){
		yield return new WaitForSeconds(1.5f);
		GameObject b = (GameObject)Instantiate(bullet, transform.position) + new Vector3(0,15,0), heroes[i].transform rotation);
		b.GetComponent<Bullet>().setTarget(heroes[i].transform);
		shoot = true;

		if(shoot){
		break;
		}

	}

}
	if (shoot){
		continue;
	}

 */