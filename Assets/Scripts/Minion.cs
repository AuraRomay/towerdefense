﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Minion : MonoBehaviour {
	private NavMeshAgent nav;
	public bool tagged = false;
	public bool ded = false;
	public GameObject[] Towers;

	public int vida;
	// Use this for initialization
	void Start () {
		nav = GetComponent<NavMeshAgent>();
	}

	// Update is called once per frame
	void Update () {
		Tower2 target=null;
		for (int i = 0; i < Towers.Length; i++)
		{
			Tower2 t=Towers[i].GetComponent<Tower2>();
			if (t.state == 1)
			{
				target = t;
				break;
			}
		}
		if (target != null) {
			nav.SetDestination(target.gameObject.transform.position);
		}

		if (vida <= 0) {
			ded = true;
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.transform.tag == "bullet") {
			Debug.Log ("colision at " + other.name);
			Destroy(other.gameObject);
			vida -= other.GetComponent<Bullet>().damage;
		}
	}
		
}
